
## Messaging system for Dark Crystal Web3

The basic implementation involves users communicating out of band a several points in the process. This works but greatly hinders usability and security.

Wallet-to-wallet encrypted messaging is being discussed in the Web3 community and would solve this problem but there is currently no widely adopted standard.

### The messages

#### Backup process

- Secret owner asks recovery partner for encryption public key - lets call this `publicKeyRequest`
- Recovery partner sends the public key to the secret owner - lets call this `publicKeyResponse`

#### Recovery process

- Secret owner asks recovery partner to retrieve a share - lets call this `shareRequest`
- Recovery partner sends the share to the secret owner - lets call this `shareResponse`

It is possible to encrypt all of these messages, except `publicKeyRequest`. This message would not need to contain any data, but if we were to publish it to a server, anyone with access to that server could assume who was being chosen as a recovery partner.

### Possible protocols we could use (all apart from Magic Wormhole are libp2p based)

#### [Waku v2](https://docs.wakuconnect.dev/)

- Works in the browser over websockets
- Offers 'relay' nodes for real time communication as well as 'store' nodes which archive messages
- Messages are stored for 30 days by nodes implementing the 'store' protocol.
- Storage works on a best effort and propagation between nodes is not guaranteed.
- Having experimented with using it, the relay protocol is very reliable but the storage propagation is not. 
- This means things would work well if the secret owner and recovery partner can both use the dapp at the same time to return a share, but otherwise things will be tricky.
- Requiring both users to use the app synchronously would give us a great security advantage (as we could do handshaking for forward secrecy). But is it too much of a hindrance to usability?
- There is an [example app that has encrypted messaging](https://js-waku.wakuconnect.dev/examples/eth-pm). It works the opposite way to how we make keypairs: first a random keypair is generated, then it is signed with the wallet keypair and 'broadcast' - where as we first make a signature and derive a keypair from it.
- Our waku experiment app: [deployed](https://dark-crystal-web3.gitlab.io/dark-crystal-web3-transport) [source](https://gitlab.com/dark-crystal-web3/dark-crystal-web3-transport)

#### [Ceramic](https://ceramic.network/)

- Used by 3box
- 'Streams' are append-only log stored as a DAG on IPLD
- Stream types support [DIDs](https://www.w3.org/TR/did-core/)

#### [libp2p-webrtc-star](https://github.com/libp2p/js-libp2p-webrtc-star)

- Allows peers to connect to each other over WebRTC using a signalling server for discovery 
- Publicly available rendezvous servers are for testing and experimentation only, so we would have to run our own

#### [peer-base](https://github.com/peer-base/peer-base)

#### [magic wormhole](https://magic-wormhole.readthedocs.io/en/latest/)

Magic wormhole is an application and protocol for secure file transfer. The protocol is extendable to allow arbitrary application-level messages to be relayed through their rendezvous servers. It uses PAKE (password authenticated key exchange) to establish a secure connection based on a short human-readable 'wormhole code' which is sent by the user out-of band. Since connections to the rendezvous server are made over websockets, it is possible to implement a browser client.

### Other notes

Public map of hash(ethaddress) to encryption public key

This gives a slight privacy advantage but it is still possible to find all users but scraping addresses from the blockchain and trying them.

encrypttorecoverypartner(encryptionPk + signature)
