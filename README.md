# Dark Crystal Web3

**A social recovery system built on Ethereum with a focus on usability.**

Many existing social backup / recovery systems rely on the recovery partners needing to install particular software, and to safeguard some secret data. This makes it harder to find recovery partners, as they may be unwilling or unable to install the software or take responsibility for holding the data. There is also a danger that they loose the data, or disclose it to someone else (willingly or unwillingly). We propose a system where the recovery partners do not need to install software nor take custody of any additional data, provided they have a personal Ethereum wallet.

The system consists of two components:

- A web-based dAPP for use by the recovery partners which makes calls to the web3 API to generate signatures which encryption keypairs are derived from, as well as retrieve and decrypt shares from the contract during recovery.

- An offline-first software component for use by the person making a backup. Given the secret and a list of public keys, a set of encrypted shares are generated which can then by published to the blockchain. Being offline-fist, this program does not do the actual publishing, but generates a dapp link containing the encrypted payload. This can then be transferred to any browser with a web3 extension (for example via a QR code).

This system is based on a scheme proposed by [Vitalik Buterin on ethresear.ch](https://ethresear.ch/t/m-of-n-secret-sharing-with-pre-known-shares/10074) under the subheading 'alternative mechanism for the social recovery use case'.

# Design goals

- Make things as easy as possible for the recovery partners.
- At the protocol level, allow any type of secret to be backed up and recovered. But have the possibility to integrate with particular systems with particular secret types.
- Make it possible for the secret owner to create the backup on an offline / air-gapped device if they wish.

# The basic process

## Setup process

1. The secret-owner chooses a set of recovery partners and asks each of them for their public encryption keys.
2. The recovery partners generate these keys by visiting the dAPP website, and choosing an option 'generate encryption key'. This works by them signing a hash of the protocol name and version number, using a browser extension wallet. A Curve25519 keypair is derived from this signature, and the public key is displayed to the user, to be given to the secret-owner.
3. The secret owner enters their secret, the list of public keys of their recovery partners, and some identifier for themselves into the offline-first software. The program generates a set of Shamir's shares and encrypts each share to one of the recovery partner's public keys. The program also generates a 'lookup key' for each share by hashing some identifying information for themself or for the secret (such as their long term ethereum address) together with the encryption public key of each recipient.
4. The encrypted shares and lookup keys are included in a dAPP link, which when followed calls a contract function which stores them in a key-value mapping.

## Recovery process

1. The secret owner contacts each of the recovery partners, and asks them to recover their share. They give them the identifier used in their lookup key. Optionally, they can also give an encryption public key to use when returning the share.
2. The recovery partners visit the dAPP website again, choose an option to recover a share, and enter the identifier given to them by the secret owner.
3. The dAPP re-generates the encryption keypair used in the setup process.
4. The dAPP computes the lookup key by hashing the identifier together with their public encryption key. 
5. The dAPP makes a call to the contract with the lookup key and the contract returns the associated encrypted share.
6. If successfully decrypted, the share is displayed to the recovery partner, and they can send it back to the secret owner. If they were given an encryption public key, the share is re-encrypted using this key, so it can be returned over an insecure channel.
7. On receiving a share, the secret owner may enter it into the offline-first software. The secret data is recovered once the threshold is met.

# Encoding of the secret

To make it easy to encode and decode secrets of different formats, the first byte of the secret represents its format. This is particularly useful for [BIP39](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki) seed phrases, as it gives a much shorter secret than if they are encoded as a string.

Currently implemented:

- `0x00` - Raw bytes (will be displayed as hex on recovery)
- `0x01` - Utf-8
- `0x02` - A BIP39 seed phrase (displayed in English by default)

Proposed but not yet implemented:

- `0x03` - A descriptive label (Utf-8), followed by a secret (of any type)

## The contract

[Source code](https://gitlab.com/dark-crystal-web3/dark-crystal-contracts/-/blob/main/contracts/Secrets.sol)

![Lookup key generation](./img/lookup-key.png)

- A function which takes an array of lookup-keys and encrypted shares and stores them in a mapping
- A function which takes a lookup-key and returns the associated encrypted share

# Implementation design of individual software components

## Browser-based dAPP for use by recovery partners

![Screenshot of dapp for recovery partners](./img/dc-dapp-screenshot.png)
- Deployed to: [web3.darkcrystal.pw](https://web3.darkcrystal.pw/)
 
### Dependencies

- [`web3.js`](https://github.com/ChainSafe/web3.js)
- [`sodium-javascript`](https://github.com/sodium-friends/sodium-javascript)
- React

#### Setup process

- On 'generate encryption key' button clicked:
- Take hash of protocol name and version number as a message
- Generate its signature with [`web3.eth.sign`](https://web3js.readthedocs.io/en/v1.5.2/web3-eth.html#sign)
- Hash the signature to 32 bytes with [`genericHash`](https://sodium-friends.github.io/docs/docs/generichashing#crypto_generichash) (Blake2b - but we could instead use keccak256 to be more fitting with Ethereum)
- Use the result as seed input for [`crypto_box_seed_keypair`](https://sodium-friends.github.io/docs/docs/keyboxencryption#crypto_box_seed_keypair) (X25519)
- Output the generated public key

#### Recovery process

- Identifier is given in a text input box, and 'recover share' button is clicked.
- Re-generate encryption keypair as in the setup process.
- Create lookup key by hashing encryption public key together with given identifier.
- Make a call to the contract's getter function with the lookup key, to retrieve the encrypted share.
- Decrypt the share and display it if the decryption was successful.


## Offline-first program for use by secret owner

![Screenshot of web interface for secret owner](./img/dc-web3-backup-screenshot.png)

A command line interface, also available as a web UI which can be served to localhost.

It is also deployed to: [web3-backup.darkcrystal.pw](https://web3-backup.darkcrystal.pw/)

### Dependencies (Rust)

- [`dark-crystal-key-backup-rust`](https://gitlab.com/dark-crystal-rust/dark-crystal-key-backup-rust) for secret encryption and sharing, which internally uses [`sharks`](https://docs.rs/sharks/0.5.0/sharks).
- [`x25519-dalek`](https://docs.rs/x25519-dalek/1.2.0/x25519_dalek/) for asymmetric encryption which is a dependency of [`crypto_box`](https://docs.rs/crypto_box/0.7.0/crypto_box).
- [`bip39`](https://docs.rs/bip39/1.0.1/bip39/) for parsing seed phrases.
- [`rlp`](https://docs.rs/rlp/0.5.1/rlp) ([Recursive Length Prefix](https://eth.wiki/en/fundamentals/rlp)) for serialising the backup payload.
- [`qrcode_generator`](https://docs.rs/qrcode-generator/4.1.2/qrcode_generator) for QR codes as PNG/SVG, or [`qr_code`](https://docs.rs/qr_code/1.1.0/qr_code/) for unicode strings.
- [`clap`](https://docs.rs/clap/2.33.3/clap/) for the CLI and [`yew`](https://yew.rs/) for the web interface

#### Setup process

- Secret is encrypted with authenticated symmetric encryption and the key taken as input for the secret sharing.
- Shares encrypted for each recovery partner.
- Function call created containing ciphertext, encrypted shares and lookup keys.
- Payload link encoded (using RLP for serialisation) and displayed (also as QR code).
- When the payload link is opened when online, the encrypted share payload is published.

#### Recovery process

- Given a set of decrypted shares (with ciphertext appended), attempt recovery.
- Ignore duplicate or mismatched shares.
- On successful recovery, output secret data using encoding format given as the first byte of the secret.

# Messaging / Transport

Encrypted messages between the secret owner and recovery partners are published using [Waku v2](https://docs.wakuconnect.dev).

There are 3 message types:

- `PubKey` - announcement of public encryption key by recovery partner
- `ShareRequest` - a request from the secret owner for a share from the recovery partner
- `Share` - a response to a share request containing a share

[Dark Crystal Web3 Transport](https://gitlab.com/dark-crystal-web3/dark-crystal-web3-transport) has a more detailed explanation of these messages, as well as the [protobuf schema](https://gitlab.com/dark-crystal-web3/dark-crystal-web3-transport/-/blob/main/lib/messages.proto) and [an example app demonstrating them](https://dark-crystal-web3.gitlab.io/dark-crystal-web3-transport)

### Screenshot of example transport app

![Screenshot of example app](./img/screenshot-dc-web3-transport-test-app.png)

Discussion about the [choice of transport protocol is here](./transport.md)

# Issues

- What happens when the same lookup key and recovery partner are used together a second time? Current strategy is to 'clobber' the existing entry. Another option is to store both and return an array of shares on retrieval. The recovery software will gracefully handle being given additional shares from mismatched sets.
- Need a way to return shares securely from the recovery partner to the secret owner.
- In other implementations of Dark Crystal, we include a signature with each share before it is encrypted. This lets us verify the integrity of shares on recovery. Here we decided not to because of the cost of on-chain storage. Signatures would add 64 bytes per share.
